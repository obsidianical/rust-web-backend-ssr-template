with import <nixpkgs> { 
  overlays = [ 
    (import (builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz")) 
  ];
};
pkgs.mkShell {
  buildInputs = with pkgs; [ 
    cargo 
    (rust-bin.stable.latest.default.override {
      extensions = [ "rust-src" ];
      targets = [ "wasm32-unknown-unknown" ];
    })
    pkg-config
    wasm-bindgen-cli
    trunk
    nodePackages.sass
    binaryen
  ]; 
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
