use yew::prelude::*;

pub mod web;
//pub mod tracking;

#[function_component]
pub fn App() -> Html {
    html! {
        <main>
            <h1>{ "Hello world!" }</h1>
            <p>{ "meowmeowmeowmeow" }</p>
        </main>
    }
}
