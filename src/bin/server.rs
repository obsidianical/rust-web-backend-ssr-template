use assistant_ish_thing::{App, web::server};
use log::info;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    simple_logger::init_with_level(log::Level::Debug)?;
    info!("Logger initalized");

    server().await?;
    Ok(())
}

