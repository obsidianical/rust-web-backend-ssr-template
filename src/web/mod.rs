
use std::io;

use actix_files::Files;
use actix_web::{get, Responder, HttpResponse, http::header::ContentType, HttpServer};
use log::info;
use yew::ServerRenderer;

use crate::App;

pub async fn server() -> io::Result<()> {
    info!("Starting web server...");

    HttpServer::new(|| {
        actix_web::App::new()
            .service(main_page)
            .service(Files::new("/", "./dist"))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

#[get("/")]
async fn main_page() -> impl Responder {
    info!("Main page has been requested!");
    
    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(render().await)
}

async fn render() -> String {
    let renderer = ServerRenderer::<App>::new();
    info!("Renderer initialized");
    let content = renderer.render().await;
    info!("Page rendered.");

    // get index_html and split it to put it around rendered content
    let index_html_raw = tokio::fs::read_to_string("./dist/index.html")
        .await
        .expect("failed to read index.html");
    let (index_html_before, index_html_after) = index_html_raw.split_once("<body>").unwrap();
    let mut index_html_before = index_html_before.to_owned();
    index_html_before.push_str("<body>");
    let index_html_after = index_html_after.to_owned(); 

    format!("{}{}{}", index_html_before, content, index_html_after)
}

